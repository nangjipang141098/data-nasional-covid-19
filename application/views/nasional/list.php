<div id="content-wrapper">

      <div class="container-fluid">

      
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Kasus Covid-19 di Indonesia</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                      <tr>
                        <th scope="col">Provinsi</th>
                        <th scope="col">Positif</th>
                        <th scope="col">Sembuh</th>
                        <th scope="col">Meninggal</th>
                      </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach ($recDetails as $datadetails) : ?>
                            <tr>
                                <td><?php echo $datadetails['attributes']['Provinsi']; ?></td>
                                <td><?php echo $datadetails['attributes']['Kasus_Posi']; ?></td>
                                <td><?php echo $datadetails['attributes']['Kasus_Semb']; ?></td>
                                <td><?php echo $datadetails['attributes']['Kasus_Meni']; ?></td>
                            </tr>
                <?php endforeach ?>
              
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>