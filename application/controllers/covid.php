<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Covid extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        // Summary (Indonesia)
        $url = "https://api.kawalcorona.com/indonesia/";
        $response = file_get_contents($url);
        // var_dump($response);
        $objSum = json_decode($response, true);
      //  $data['recSum'] = $objSum;
 
        // Details (Indonesia)
        $url = "https://api.kawalcorona.com/indonesia/provinsi/";
        $response = file_get_contents($url);
        // var_dump($response);
        $objDetail = json_decode($response, true);
        //$data['recDetails'] = ;

        

		$data = array('title' => 'Data Covid 19 Nasional',
                      'content' => 'nasional/list',
                      'recSum' => $objSum,
                      'recDetails' => $objDetail
                     );
                     
	
    	$this->load->view('tamplate/wrapper', $data, FALSE);
	}
}
